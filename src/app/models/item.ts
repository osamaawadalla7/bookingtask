export interface Item {
  userId: number,
  id: number,
  title: string,
  body: string,
  image: string,
  price: number,
  address: string
}
