import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemBox2Component } from './item-box2.component';

describe('ItemBox2Component', () => {
  let component: ItemBox2Component;
  let fixture: ComponentFixture<ItemBox2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemBox2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemBox2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
