import { Component, Input, OnInit } from '@angular/core';
import { Item } from 'src/app/models/item';

@Component({
  selector: 'app-item-box2',
  templateUrl: './item-box2.component.html',
  styleUrls: ['./item-box2.component.scss']
})
export class ItemBox2Component implements OnInit {

  @Input() item: Item;

  constructor() { }

  ngOnInit(): void {
  }

}
